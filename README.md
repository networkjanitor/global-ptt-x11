# global-ptt

unmutes/mutes default pulseaudio source on xinput2 RawKeyPress/RawKeyRelease event.

keypresses are detected non-blocking (via XISelectEvents), so this works just like all the integrated PTT features in teamspeak/mumble/discord. 

prior to writing this, I had no idea how any of this works, which also means the code is probably suboptimal, might crash, might do other stuff it's not supposed to, might not handle cases well, which I haven't stumbled upon yet.

keycode parameter can be determined by looking at `xmodmap -pke`, e.g. KP_Multiply is 63, so you would call `global-ptt 63`.
