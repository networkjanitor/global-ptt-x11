extern crate x11;

use std::process::Command;
use std::ptr;
use x11::xinput2;
use x11::xlib;

use structopt::StructOpt;

#[derive(StructOpt)]
struct Config {
    /// i32 keycode, which should trigger the action of unmute/mute on press/release
    keycode: i32,
}

fn main() {
    let config = Config::from_args();

    let display = unsafe { (xlib::XOpenDisplay)(ptr::null()) };
    let root = unsafe { (xlib::XDefaultRootWindow)(display) };

    // setup mask
    let mut mask = [0u8; (xinput2::XI_LASTEVENT as usize + 7) / 8];
    xinput2::XISetMask(&mut mask, xinput2::XI_RawKeyPress);
    xinput2::XISetMask(&mut mask, xinput2::XI_RawKeyRelease);

    let mut events = [xinput2::XIEventMask {
        deviceid: xinput2::XIAllMasterDevices,
        mask_len: mask.len() as i32,
        mask: &mut mask[0] as *mut u8,
    }];

    // select events
    unsafe {
        (xinput2::XISelectEvents)(
            display,
            root,
            &mut events[0] as *mut xinput2::XIEventMask,
            events.len() as i32,
        )
    };

    // receive events
    loop {
        let mut event = xlib::XEvent { pad: [0; 24] };
        unsafe { (xlib::XNextEvent)(display, &mut event) };

        // skip non-GenericEvents, because mumble does this as well
        if event.get_type() != xlib::GenericEvent {
            continue;
        }

        // create generic event cookie and load data
        let mut cookie = xlib::XGenericEventCookie::from(event);
        unsafe { (xlib::XGetEventData)(display, &mut cookie) };

        // make event magic
        let xev: &xinput2::XIRawEvent = unsafe { &*(cookie.data as *const _) };

        // evaluate XIRawEvent and take action
        match xev.evtype {
            xinput2::XI_RawKeyPress => {
                if (*xev).detail == config.keycode {
                    key_pressed()
                }
            }
            xinput2::XI_RawKeyRelease => {
                if (*xev).detail == config.keycode {
                    key_released()
                }
            }
            _ => (),
        }
    }
}

fn key_pressed() {
    println!("Key pressed!");
    Command::new("pactl")
        .arg("set-source-mute")
        .arg("@DEFAULT_SOURCE@")
        .arg("0")
        .spawn()
        .expect("could not mute default source");
}
fn key_released() {
    println!("Key released!");
    Command::new("pactl")
        .arg("set-source-mute")
        .arg("@DEFAULT_SOURCE@")
        .arg("1")
        .spawn()
        .expect("could not mute default source");
}
